#include <Wire.h>
#include <TEA5767Radio.h>

byte byteBaca; 
float freq;

TEA5767Radio radio = TEA5767Radio();

void setup() {
  Wire.begin();
  Serial.begin(9600);
   radio.setFrequency(106.1);
}

void loop() {
  if (Serial.available()) {
    freq = Serial.parseFloat();
    radio.setFrequency(freq);
    Serial.println(freq);
  }
}
